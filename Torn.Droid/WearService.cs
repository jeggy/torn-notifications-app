using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Torn.Model;
using Torn.Services;

namespace Torn.Droid
{
    [Service]
    public class WearService : Service, IDataApiDataListener
    {
        private const string Path = "/Torn/data";
        private const string Tag = "WearService";

        public GoogleApiClient Client;

        public TornUser SelectedTornUser;

        private bool Running = true;
        private Handler _handler;

        public TornUser[] Users = {new TornUser
            {
                Id = 1526723,
                Username = "Jeggy",
                ApiKey = "QblVM"
            },
            new TornUser{
                Id = 432440,
                Username = "Chieftain",
                ApiKey = "C9yA2"
            }
        };

        public override void OnCreate()
        {
            base.OnCreate();
            _handler = new Handler();
            Log.Debug(Tag, "OnCreate");
        }

        private void RunOnUiThread(Action runnable)
        {
            _handler.Post(runnable);
        }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(Tag, "OnBind");
            return null;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {

            StartWearService();
            return StartCommandResult.Sticky;
        }

        private void StartWearService()
        {
            Log.Debug(Tag, "StartWearService");
            Client = new GoogleApiClient.Builder(this.ApplicationContext)
                    .AddApi(WearableClass.API)
                    .Build();
            Client.Connect();

            WearableClass.DataApi.AddListener(Client, this);

            UpdateUserList();

            UpdateTorn();
        }


        private void UpdateUserList()
        {
            Log.Debug(Tag, "UpdateUserList");
            var request = PutDataMapRequest.Create(Path);

            var ids = new List<Integer>();
            var usernames = new List<string>();

            foreach (var t in Users)
            {
                usernames.Add(t.Username);
                ids.Add(new Integer(t.Id));
            }

            request.DataMap.PutStringArrayList("Usernames", usernames);
            request.DataMap.PutIntegerArrayList("Ids", ids); 
            request.SetUrgent();

            WearableClass.DataApi.PutDataItem(Client, request.AsPutDataRequest());
        }

        public void OnDataChanged(DataEventBuffer dataEvents)
        {
            foreach (var e in dataEvents)
            {
                if (!e.IsDataValid) continue;

                var map = DataMapItem.FromDataItem(e.DataItem);
                if (map.DataMap.ContainsKey("CurrentUserIndex"))
                {
                    UserIndexUpdated(map.DataMap.GetInt("CurrentUserIndex"));
                }
            }
        }

        private void UserIndexUpdated(int selectedIndex)
        {
            SelectedTornUser = Users[selectedIndex];
        }

        private void UpdateTorn()
        {
            new Thread(() =>
            {
                while (Running)
                {
                    if (SelectedTornUser?.ApiKey != null)
                    {
                        var user = TornService.GetUserDataAsync(SelectedTornUser.ApiKey).Result;

                        var request = PutDataMapRequest.Create(Path);
                        request.DataMap.PutString("Username", user.Username);
                        request.DataMap.PutInt("Energy", user.Energy);
                        request.DataMap.PutInt("MaximumEnergy", user.MaximumEnergy);
                        request.DataMap.PutInt("Happy", user.Happy);
                        request.DataMap.PutInt("MaximumHappy", user.MaximumHappy);
                        request.DataMap.PutInt("Nerve", user.Nerve);
                        request.DataMap.PutInt("MaximumNerve", user.MaximumNerve);
                        request.SetUrgent();

                        WearableClass.DataApi.PutDataItem(Client, request.AsPutDataRequest());
                    }
                    else
                    {
                        Log.Debug(Tag, "No ApiKey.");
                        this.RunOnUiThread(() =>
                        {
                            Toast.MakeText(this, "No ApiKey.", ToastLength.Long).Show();
                        });
                    }
                    Thread.Sleep(10000);
                }
            }).Start();
        }

        public void SendNotification(string text)
        {
            
        }
    }
}
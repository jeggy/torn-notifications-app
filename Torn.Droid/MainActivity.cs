﻿using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace Torn.Droid
{
    [Activity(Label = "Torn.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private int _count = 1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            var button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += delegate { button.Text = $"{_count++} clicks!"; };
            StartWearService();
        }

        public void StartWearService()
        {
            Log.Debug("MainActivity", "Starting Wear Service");
            StartService(new Intent(this, typeof(WearService)));
        }
    }
}


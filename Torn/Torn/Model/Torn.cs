﻿using Newtonsoft.Json;

namespace Torn.Model
{
    public class TornUser
    {
        public int Id;
        public string Username;

        public string ApiKey;

        public int Energy;
        public int MaximumEnergy;
        public int Happy;
        public int MaximumHappy;
        public int Nerve;
        public int MaximumNerve;

        public string TravelDestination;
        public int TravelTimeLeft;

        // Error
        public string ErrorText;
    }
}

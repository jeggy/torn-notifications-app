﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Torn.Model;

namespace Torn.Services
{
    public class TornService
    {
        public static async Task<TornUser> GetUserDataAsync(string apiKey)
        {
            var selections = "bars,travel,profile";
            var requestData = await ApiService.GetDataFromServiceAsync("https://api.torn.com/user/", "?selections=" + selections+"&key=" + apiKey);

            if(requestData == null)
            {
                return new TornUser {
                    ErrorText = "Couldn't connect to the internet."
                };
            }

            if (requestData["error"] != null)
            {
                return new TornUser
                {
                    ErrorText = requestData["error"]["error"].Value<string>()
                };
            }

            return new TornUser
            {
                Id = requestData["player_id"].Value<int>(),
                Username = requestData["name"].Value<string>(),

                Energy = requestData["energy"]["current"].Value<int>(),
                MaximumEnergy = requestData["energy"]["maximum"].Value<int>(),
                Happy = requestData["happy"]["current"].Value<int>(),
                MaximumHappy = requestData["happy"]["maximum"].Value<int>(),
                Nerve = requestData["nerve"]["current"].Value<int>(),
                MaximumNerve = requestData["nerve"]["maximum"].Value<int>(),

                TravelDestination = requestData["travel"]["destination"].Value<string>(),
                TravelTimeLeft = requestData["travel"]["time_left"].Value<int>()
            };

        }
    }
}

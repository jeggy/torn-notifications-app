﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Util;

namespace Torn.Services
{
    public class ApiService
    {
        private static string TAG = "ApiService";

        public static async Task<JObject> GetDataFromServiceAsync(string url, string parameter)
        {
            var client = new HttpClient { BaseAddress = new Uri(url) };
            client.Timeout = TimeSpan.FromSeconds(5);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response;
            try
            {
                response = await client.GetAsync(parameter);
            }
            catch (Exception e)
            {
                Log.Debug(TAG, e.Message);
                return null;
            }
            Log.Debug(TAG, "Continue");
            

            if (response.IsSuccessStatusCode)
            {
                return JObject.Parse(response.Content.ReadAsStringAsync().Result);
            }
            return null;
        }
    }
}

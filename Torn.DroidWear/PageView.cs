using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Torn.DroidWear.Adapter;
using Torn.DroidWear.Fragments;

namespace Torn.DroidWear
{
    public class PageView : Activity
    {
        public MainFragment MainFrag;
        public SettingsFragment SettingsFrag;



        // https://developer.android.com/samples/JumpingJack/src/com.example.android.wearable.jumpingjack/MainActivity.html
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            var mPager = FindViewById<ViewPager>(Resource.Id.pager);
            var adapter = new ApplicationPageAdapter(FragmentManager);
            MainFrag = new MainFragment();
            SettingsFrag = new SettingsFragment();
            adapter.AddFragment(MainFrag);
            adapter.AddFragment(SettingsFrag);

            mPager.Adapter = adapter;
        }
    }
}
﻿using System;
using Android.App;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.Wearable.Views;
using System.Collections.Generic;
using System.Net;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using Android.Util;
using Android.Gms.Common;
using Android.Support.V4.View;
using Java.Lang;
using Torn.DroidWear.Adapter;
using Torn.DroidWear.Fragments;

namespace Torn.DroidWear
{
    [Activity(Label = "Torn.DroidWear", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : PageView, IDataApiDataListener, GoogleApiClient.IConnectionCallbacks,
        GoogleApiClient.IOnConnectionFailedListener
    {
        private const string TAG = "MainActivity";
        private const string Path = "/Torn/data";

        public GoogleApiClient Client;
        private TornData _tornData;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Log.Debug(TAG, "OnCreate");

            // Google Wear Api Client
            Client = new GoogleApiClient.Builder(this)
                .AddApi(WearableClass.API)
                .Build();
            if (!Client.IsConnected)
                Client.Connect();
            WearableClass.DataApi.AddListener(Client, this);

            // Settings events
            SettingsFrag.Adapter.OnItemClick += (s, e) =>
            {
                UpdateId(((UserListEventArgs)e).ViewHolder.Index);
            };

            AskForData();
        }

        public void UpdateView()
        {
            Log.Debug(TAG, "UpdateView");
            if (_tornData == null) return;

            SettingsFrag.SetCurrentSelectionText("< " + _tornData.Username);
            MainFrag.Updateview(_tornData);
        }

        public void UpdateId(int index)
        {
            var request = PutDataMapRequest.Create(Path);
            request.DataMap.PutInt("CurrentUserIndex", index);
            request.SetUrgent();
            WearableClass.DataApi.PutDataItem(Client, request.AsPutDataRequest());
            Log.Debug(TAG, "Updated CurrentUserIndex: " + index);
        }

        public void OnDataChanged(DataEventBuffer dataEvents)
        {
            Log.Debug(TAG, "OnDataChanged");

            foreach (var e in dataEvents)
            {
                Log.Debug(TAG, "OnDataChanged Loop | " + DataMapItem.FromDataItem(e.DataItem).ToString());
                if (e.IsDataValid)
                {
                    HandleDataMapItem(DataMapItem.FromDataItem(e.DataItem));
                }
            }
        }

        private void HandleDataMapItem(DataMapItem dataMapItem)
        {
            Log.Debug(TAG, "HandleDataMapItem | " + dataMapItem.DataMap);
            var map = dataMapItem.DataMap;

            if (map.ContainsKey("Energy"))
            {
                if (_tornData == null)
                {
                    _tornData = new TornData
                    {
                        Username = map.GetString("Username"),
                        Energy = map.GetInt("Energy"),
                        MaximumEnergy = map.GetInt("MaximumEnergy"),
                        Nerve = map.GetInt("Nerve"),
                        MaximumNerve = map.GetInt("MaximumNerve"),
                        MaximumHappy = map.GetInt("MaximumHappy"),
                        Happy = map.GetInt("Happy")
                    };
                }
                else
                {
                    _tornData.Username = map.GetString("Username");
                    _tornData.Energy = map.GetInt("Energy");
                    _tornData.MaximumEnergy = map.GetInt("MaximumEnergy");
                    _tornData.Nerve = map.GetInt("Nerve");
                    _tornData.MaximumNerve = map.GetInt("MaximumNerve");
                    _tornData.MaximumHappy = map.GetInt("MaximumHappy");
                    _tornData.Happy = map.GetInt("Happy");
                }
                UpdateView();
            }
            else if (map.ContainsKey("Usernames"))
            {
                var usernames = map.GetStringArrayList("Usernames");
                var ids = map.GetIntegerArrayList("Ids");

                SettingsFrag.ClearDataSet();

                for (var i = 0; i < usernames.Count; i++)
                {
                    SettingsFrag.AddToDataSet(new TemplateViewHolder
                    {
                        Index = i,
                        Title = usernames[i],
                        Subtitle = ids[i].ToString()
                    });
                }

                SettingsFrag.Spinner(false);
            }
        }

        public async void AskForData()
        {
            Log.Debug(TAG, "AskForData");
            Log.Debug(TAG, "Connected: " + Client.IsConnected + ", Is Connecting:" + Client.IsConnecting);

            if (!Client.IsConnected)
            {
                Log.Debug(TAG, "Reconnecting");
                Client.Connect();
            }

            var items = await WearableClass.DataApi.GetDataItemsAsync(Client);

            foreach (var node in items)
            {
                if (node.IsDataValid)
                {
                    HandleDataMapItem(DataMapItem.FromDataItem(node));
                }
                else
                {
                    Log.Debug(TAG, "Node not valid");
                }
            }
        }

        // -- OnConnect n' stuff
        public void OnConnected(Bundle connectionHint)
        {
            Log.Debug(TAG, "OnConnected");
            WearableClass.DataApi.AddListener(Client, this);

            AskForData();
        }

        public void OnConnectionSuspended(int cause)
        {
            Log.Debug(TAG, "OnConnectionSuspend");
            WearableClass.DataApi.RemoveListener(Client, this);
        }

        public void OnConnectionFailed(ConnectionResult result)
        {
            Log.Debug(TAG, "OnConnectionFailed: " + result.ErrorCode);
        }

    }
}



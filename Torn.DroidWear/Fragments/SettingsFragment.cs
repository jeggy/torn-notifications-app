using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Wearable.Views;
using Android.Util;
using Android.Views;
using Android.Widget;
using Torn.DroidWear.Adapter;

namespace Torn.DroidWear.Fragments
{
    public class SettingsFragment : Fragment
    {
        private const string TAG = "SettingsFragment";

        public UserListViewAdapter Adapter;
        private readonly List<TemplateViewHolder> _dataSet;
        private ProgressSpinner _spinner;
        private TextView _current_selection;

        public SettingsFragment()
        {
            _dataSet = new List<TemplateViewHolder>();
            Adapter = new UserListViewAdapter(_dataSet);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.Settings, container, false);


            var listView = view.FindViewById<WearableListView>(Resource.Id.settings_list);
            _spinner = view.FindViewById<ProgressSpinner>(Resource.Id.loading_spinner);
            _current_selection = view.FindViewById<TextView>(Resource.Id.current_selection);
            listView.SetAdapter(Adapter);

            return view;
        }

        public void ClearDataSet()
        {
            Log.Debug(TAG, "ClearDataSet");
            _dataSet.Clear();
            Adapter.NotifyDataSetChanged();
        }

        public void AddToDataSet(TemplateViewHolder item)
        {
            Log.Debug(TAG, "AddToDataSet");
            _dataSet.Add(item);
            Adapter.NotifyDataSetChanged();
        }

        public void SetCurrentSelectionText(string text)
        {
            _current_selection.Text = text;
        }

        public void Spinner(bool visible)
        {
            Log.Debug(TAG, "Spinner: "+visible);
            _spinner.Visibility = visible ? ViewStates.Visible : ViewStates.Invisible;
        }
    }
}
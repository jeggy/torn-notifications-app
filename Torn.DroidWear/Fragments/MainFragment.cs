using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Wearable.Views;
using Android.Util;
using Android.Views;
using Android.Widget;
using Torn.DroidWear.Adapter;

namespace Torn.DroidWear.Fragments
{
    public class MainFragment : Fragment
    {

        public UserListViewAdapter Adapter;
        private readonly List<TemplateViewHolder> _dataSet;

        public MainFragment()
        {
            _dataSet = new List<TemplateViewHolder>
            {
                new TemplateViewHolder
                {
                    Title = "Energy",
                    Subtitle = "0/100"
                },
                new TemplateViewHolder
                {
                    Title = "Happy",
                    Subtitle = "0/100"
                },
                new TemplateViewHolder
                {
                    Title = "Nerve",
                    Subtitle = "0/10"
                }
            };
            Adapter = new UserListViewAdapter(_dataSet);
            Adapter.NotifyDataSetChanged();

            Adapter.OnItemClick += (s, e) =>
            {
                Log.Debug(Tag, "TEST");
            };
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.UserView, container, false);


            var listView = view.FindViewById<WearableListView>(Resource.Id.main_list);

            listView.SetAdapter(Adapter);

            return view;
        }

        public void Updateview(TornData tornData)
        {
            
            _dataSet[0].Subtitle = tornData.Energy + "/" + tornData.MaximumEnergy;
            _dataSet[1].Subtitle = tornData.Happy + "/" + tornData.MaximumHappy;
            _dataSet[2].Subtitle = tornData.Nerve + "/" + tornData.MaximumNerve;

            Adapter.NotifyDataSetChanged();
        }
    }
}
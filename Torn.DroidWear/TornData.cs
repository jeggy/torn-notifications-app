namespace Torn.DroidWear
{
    public class TornData
    {
        public int Id;
        public string Username; 
        public int Energy;
        public int MaximumEnergy;
        public int Nerve;
        public int MaximumNerve;
        public int Happy;
        public int MaximumHappy;
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V13.App;
using Android.Views;
using Android.Widget;

namespace Torn.DroidWear.Adapter
{
    internal class ApplicationPageAdapter : FragmentPagerAdapter
    {
        List<Fragment> mFragments = null;
        
        public ApplicationPageAdapter(FragmentManager fm) : base(fm)
        {
            mFragments = new List<Fragment>();
        }

        public override int Count => mFragments.Count;

        public override Fragment GetItem(int position)
        {
            return mFragments[position];
        }

        public void AddFragment(Fragment fragment)
        {
            mFragments.Add(fragment);
            NotifyDataSetChanged();
        }
    }
}
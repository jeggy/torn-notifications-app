using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Support.Wearable.Views;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Torn.DroidWear.Adapter
{
    public class UserListViewAdapter : RecyclerView.Adapter
    {
        private const string TAG = "UserListViewAdapter";
        private List<TemplateViewHolder> DataSet;

        public EventHandler OnItemClick;

        public UserListViewAdapter(List<TemplateViewHolder> list)
        {
            DataSet = list;
        }

        public override int ItemCount => DataSet.Count;

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var th = holder as ItemViewHolder;
            var t = DataSet[position];
            if (th == null || t == null)
                return;
            th.BindTemplate(t);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.TemplateItem, parent, false);

            var vh = new ItemViewHolder(itemView, OnItemClick);

            return vh;
        }

        private class ItemViewHolder : WearableListView.ViewHolder
        {
            private readonly TextView _templateTitle;
            private readonly TextView _templateSubTitle;
            private readonly LinearLayout _linearLayout;

            private event EventHandler OnItemClick;

            public ItemViewHolder(View itemView, EventHandler onItemClick) : base(itemView)
            {
                _templateTitle = itemView.FindViewById<TextView>(Resource.Id.template_title);
                _linearLayout = itemView.FindViewById<LinearLayout>(Resource.Id.layout);
                _templateSubTitle = itemView.FindViewById<TextView>(Resource.Id.template_subtitle);

                OnItemClick = onItemClick;
            }

            private void OnClick(TemplateViewHolder template)
            {
                Log.Debug(TAG, "ItemViewHolder, OnClick");
                Log.Debug(TAG, "OnClick: " + template.Index);

                OnItemClick?.Invoke(this, new UserListEventArgs {ViewHolder = template});
            }

            public void BindTemplate(TemplateViewHolder template)
            {
                _templateTitle.Text = template.Title;
                _templateSubTitle.Text = template.Subtitle;
                _linearLayout.Click += (sender, e) => OnClick(template);
            }
        }
    }

    public class UserListEventArgs : EventArgs
    {
        public TemplateViewHolder ViewHolder;
    }

    public class TemplateViewHolder
    {
        public int Index;
        public string Title;
        public string Subtitle;
    }
}
﻿using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Preferences;
using Android.Support.V7.App;
using Android.Util;

namespace TornNotifications
{
    [Activity(Label = "Torn Notifications", MainLauncher = true, Icon = "@drawable/notification_icon")]
    public class MainActivity : AppCompatActivity
    {
        public const string PACKAGE_NAME = "net.jebster.TornNotifications";
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Empty);


            var fragmentTransaction = FragmentManager.BeginTransaction();

            var settingsFragment = new SettingsFragment();
            fragmentTransaction.Add(Android.Resource.Id.Content, settingsFragment, "SETTINGS_FRAGMENT");
            fragmentTransaction.Commit();

            Log.Debug("MainActivity", "Starting Torn Service");
            StartService(new Intent(this, typeof(TornBackgroundService)));
        }


        public class SettingsFragment : PreferenceFragment, ISharedPreferencesOnSharedPreferenceChangeListener
        {
            public override void OnCreate(Bundle savedInstanceState)
            {
                base.OnCreate(savedInstanceState);

                AddPreferencesFromResource(Resource.Xml.prefs);
                PreferenceScreen.SharedPreferences.RegisterOnSharedPreferenceChangeListener(this);
            }

            public override void OnResume()
            {
                base.OnResume();
                for (int i = 0; i < PreferenceScreen.PreferenceCount; ++i)
                {
                    var preference = PreferenceScreen.GetPreference(i);
                    if (preference is PreferenceGroup) {
                        var preferenceGroup = (PreferenceGroup)preference;
                        for (int j = 0; j < preferenceGroup.PreferenceCount; ++j)
                        {
                            Preference singlePref = preferenceGroup.GetPreference(j);
                            UpdatePreference(singlePref, singlePref.Key);
                        }
                    } else {
                        UpdatePreference(preference, preference.Key);
                    }
                }
            }

            private void UpdatePreference(Preference preference, string key)
            {
                if (preference == null) return;
                var sharedPrefs = PreferenceManager.SharedPreferences;

                var text = Resources.GetString(Resources.GetIdentifier(key + "Summary", "string", PACKAGE_NAME));
                if (preference is EditTextPreference)
                {
                    var editTextPref = (EditTextPreference)preference;
                    preference.Summary = text + " : " + sharedPrefs.GetString(key, editTextPref.Text);
                }

                if(preference is RingtonePreference)
                {
                    var ringtonePref = (RingtonePreference)preference;
                    var testa = Uri.Parse(sharedPrefs.GetString("NotificationSound", "default ringtone"));
                    preference.Summary = text + " : " + testa;
                }
            }

            public void OnSharedPreferenceChanged(ISharedPreferences sharedPreferences, string key)
            {
                UpdatePreference(FindPreference(key), key);
                this.Context.StartService(new Intent(this.Context, typeof(TornBackgroundService)));
            }
        }


    }

    
}


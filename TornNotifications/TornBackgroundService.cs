using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Java.Lang;
using Torn.Model;

namespace TornNotifications
{
    [Service]
    public class TornBackgroundService : Service
    {
        public const string TAG = "TornBackgroundService";

        public bool Running = true;

        private TornUser _currentTornData;
        private TornUser _lastTornData;
        private SaveData _preferences;

        private TornNotificationManager TornNotificationManager;

        public override void OnCreate()
        {
            base.OnCreate();

            TornNotificationManager = new TornNotificationManager(this);
        }


        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            Log.Debug(TAG, "OnStartCommand");
            var startLoop = _preferences == null;
            LoadPreferences();

            Log.Debug(TAG, "_preferences: " + startLoop);
            if (startLoop)
            {
                StartChecking();
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        public void LoadPreferences()
        {
            Log.Debug(TAG, "LoadPreferences");
            _preferences = Preferences.LoadData(this);
        }
        
        public void StartChecking()
        {
            Log.Debug(TAG, "StartChecking");
            new Thread(() =>
            {
                while (Running)
                {
                    Log.Debug(TAG, "Running Loop");

                    _currentTornData = Torn.Services.TornService.GetUserDataAsync(_preferences.ApiKey).Result;
                    if(_currentTornData.ErrorText != null && _currentTornData.ErrorText.Length > 0)
                    {
                        // TODO: Possible errors: Torn Api Errors, No Internet Connection.
                        // Maybe show in a notification?
                        Log.Debug(TAG, "Error message: " + _currentTornData.ErrorText);
                        Thread.Sleep(_preferences.UpdateSecs * 1000);
                        continue;
                    }


                    if(_lastTornData == null)
                    {
                        _lastTornData = _currentTornData;
                        Thread.Sleep(_preferences.UpdateSecs * 1000);
                        continue;
                    }

                    TornNotificationManager.NotificationsCheck(_preferences, _currentTornData, _lastTornData);

                    _lastTornData = _currentTornData;
                    Thread.Sleep(_preferences.UpdateSecs * 1000);
                }
            }).Start();
        }

        public override IBinder OnBind(Intent intent)
        {
            Log.Debug(TAG, "OnBind");
            return null;
        }
        
    }
}
using Android.Content;
using Android.Net;
using Android.Preferences;

namespace TornNotifications
{
    public class Preferences
    {

        public static SaveData LoadData(Context context)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(context);
            return new SaveData {
                ApiKey = prefs.GetString("ApiKey", ""),
                UpdateSecs = int.Parse(prefs.GetString("UpdateInterval", "-1")),
                StartOnBoot = prefs.GetBoolean("StartOnBoot", false),

                EnergyNotification = prefs.GetBoolean("EnergyNotification", false),
                NerveNotification = prefs.GetBoolean("NerveNotification", false),
                HappyNotification = prefs.GetBoolean("HappyNotification", false),
                TravelNotification = prefs.GetBoolean("TravelNotification", false),
                EventsNotification = prefs.GetBoolean("EventsNotification", false),

                Sound = prefs.GetBoolean("Sound", false),
                _sound = prefs.GetString("NotificationSound", "default ringtone"),
                Vibrate = prefs.GetBoolean("Vibrate", false),
                Led = prefs.GetBoolean("Led", false)
            };
        }
    }

    public class SaveData
    {
        public string ApiKey;
        public int UpdateSecs;
        public bool StartOnBoot;

        public bool EnergyNotification;
        public bool NerveNotification;
        public bool HappyNotification;
        public bool TravelNotification;
        public bool EventsNotification;

        public bool Sound;
        internal string _sound;
        public Uri UriSound { get {return Uri.Parse(_sound); } }
        public bool Vibrate;
        public bool Led;
    }
}
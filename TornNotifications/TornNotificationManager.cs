﻿using Android.App;
using Android.Content;
using Torn.Model;
using Android.Graphics;
using Android.Media;
using Android.Support.V4.App;
using Android.Util;
using Android.Net;

namespace TornNotifications
{
    internal class TornNotificationManager
    {
        private const string TAG = "TornNotificationManager";

        private Context context;
        private SaveData _cp;
        private TornUser _cu;

        public TornNotificationManager(Context context)
        {
            this.context = context;
        }

        public void NotificationsCheck(SaveData prefs, TornUser current, TornUser last)
        {
            _cp = prefs; _cu = current;
            if (prefs.EnergyNotification) // && current.Energy == current.MaximumEnergy && current.Energy != last.Energy)
                EnergyNotification(current.Energy, current.MaximumEnergy);

            if (prefs.HappyNotification && current.Happy == current.MaximumHappy && current.Happy != last.Happy)
                HappyNotification(current.Happy, current.MaximumHappy);

            if (prefs.NerveNotification && current.Nerve == current.MaximumNerve && current.Nerve != last.Nerve)
                NerveNotification(current.Nerve, current.MaximumNerve);

            if (prefs.TravelNotification && current.TravelTimeLeft == 0 && current.TravelTimeLeft != last.TravelTimeLeft)
                TravelNotification(current.TravelDestination);
        }


        private NotificationCompat.Builder BasicNotificationBuilder()
        {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .SetContentTitle("Torn")
                .SetSmallIcon(Resource.Drawable.notification_template_icon_bg);

            NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle()
                .AddLine("Energy: " + _cu.Energy + "/" + _cu.MaximumEnergy)
                .AddLine("Happy: " + _cu.Happy + "/" + _cu.MaximumHappy)
                .AddLine("Nerve: " + _cu.Nerve + "/" + _cu.MaximumNerve);
            if (_cu.TravelTimeLeft > 0)
                style.AddLine("Travel time: " + _cu.TravelTimeLeft + "s");

            var actionPendingIntent = PendingIntent.GetActivity(context, 1, 
                new Intent(Intent.ActionView)
                    .SetData(Uri.Parse("https://torn.com/"))
                , PendingIntentFlags.UpdateCurrent);
            builder.AddAction(Resource.Drawable.abc_text_cursor_material, "Open Torn.com", actionPendingIntent);

            builder.SetStyle(style);

            if (_cp.Sound)
            {
                Log.Debug(TAG, "Sound");
                builder.SetSound(_cp.UriSound);
            }

            if (_cp.Vibrate)
            {
                Log.Debug(TAG, "Vibrate");
                builder.SetVibrate(new long[] { 0, 1000, 500 });
            }
            if (_cp.Led) {
                Log.Debug(TAG, "Led");
                builder.SetLights(Color.LightGray, 2000, 5000);
            }
            return builder;
        }
        
        public void FinishNotification(NotificationCompat.Builder builder, int id)
        {
            var notification = builder.Build();

            NotificationManager notificationManager =
                context.GetSystemService(Context.NotificationService) as NotificationManager;

            notificationManager.Notify(id, notification);
        }

        /* -------------- Notification Types -------------- */

        public void EnergyNotification(int current, int max)
        {
            var builder = BasicNotificationBuilder();
            builder.SetContentText("You have " + current + "/" + max + " Energy!");
            
            FinishNotification(builder, 0);
        }

        public void HappyNotification(int current, int max)
        {
            var builder = BasicNotificationBuilder();
            builder.SetContentText("You have " + current + "/" + max + " Happy!");

            FinishNotification(builder, 1);
        }

        public void NerveNotification(int current, int max)
        {
            var builder = BasicNotificationBuilder();
            builder.SetContentText("You have " + current + "/" + max + " Nerve!");

            FinishNotification(builder, 2);
        }

        public void TravelNotification(string country)
        {
            var builder = BasicNotificationBuilder();
            builder.SetContentText("You landed in " + country);

            FinishNotification(builder, 3);
        }

    }
}
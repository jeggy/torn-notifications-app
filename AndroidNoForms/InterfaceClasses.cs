using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidNoForms
{
    public class InterfaceClasses
    {
        public InterfaceClasses()
        {
        }
    }

    public class ResultCallback : Java.Lang.Object, IResultCallback
    {
        public Action<Java.Lang.Object> OnResultAction;
        public void OnResult(Java.Lang.Object result)
        {
            OnResultAction?.Invoke(result);
        }
    }
}
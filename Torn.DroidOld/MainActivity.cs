﻿using System;
using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Torn.Services;

namespace Torn.Droid
{
    [Activity(Label = "Torn.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            A();
        }

        public async void A()
        {
            await TornService.GetUserDataAsync("QblVM");
        }
    }
}


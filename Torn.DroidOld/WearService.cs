using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Common.Apis;
using Android.Gms.Wearable;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace Torn.Droid
{
    public class WearService : WearableListenerService
    {
        private const string Path = "/Torn/data";
        private const string Tag = "WearService";

        public GoogleApiClient Client;

        public TornUser SelectedTornUser;

        public TornUser[] Users = {new TornUser
            {
                Id = 1526723,
                Username = "Jeggy",
                ApiKey = "QblVM"
            },
            new TornUser{
                Id = 432440,
                Username = "Chieftain",
                ApiKey = "C9yA2"
            }
        };

        public override void OnCreate()
        {
            base.OnCreate();
            Log.Debug(Tag, "OnCreate");

            Client = new GoogleApiClient.Builder(this.ApplicationContext)
                    .AddApi(WearableClass.API)
                    .Build();
            Client.Connect();

            UpdateUserList();

            UpdateTorn();
        }

        private void UpdateUserList()
        {
            var request = PutDataMapRequest.Create(Path);

            var indexes = new List<Integer>();
            var usernames = new List<string>();
            
            for (var i = 0; i < Users.Length; i++)
            {
                usernames.Add(Users[i].Username);
                indexes.Add(new Integer(i));
            }

            request.DataMap.PutStringArrayList("Usernames", usernames);
            request.DataMap.PutIntegerArrayList("Ids", indexes);
            request.SetUrgent();
            
            WearableClass.DataApi.PutDataItem(Client, request.AsPutDataRequest());
        }

        public override void OnDataChanged(DataEventBuffer dataEvents)
        {
            base.OnDataChanged(dataEvents);

            foreach (var e in dataEvents)
            {
                if (!e.IsDataValid) continue;

                var map = DataMapItem.FromDataItem(e.DataItem);
                if (map.DataMap.ContainsKey("CurrentUserIndex"))
                {
                    UserIndexUpdated(map.DataMap.GetInt("CurrentUserIndex"));
                }
            }
        }

        private void UserIndexUpdated(int selectedIndex)
        {
            SelectedTornUser = Users[selectedIndex];
        }

        private void UpdateTorn()
        {
            
        }
    }

    public class TornUser
    {
        public int Id;
        public string Username;
        public string ApiKey;

        public int Energy = 0;
        public int MaximumEnergy = 100;
        public int Happy = 0;
        public int MaximumHappy = 100;
        public int Nerve = 0;
        public int MaximumNervy = 10;
    }
}